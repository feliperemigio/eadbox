//
//  FRValidateForm.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
protocol FRValidateForm  : NSObjectProtocol{

    func validate() -> (valid: Bool, error : String?)
}

extension FRValidateForm {
    func isValidEmail(_ str:String?) -> (valid : Bool, error : String?) {
   
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: str){
            return (true, "" )
        }else{
             return (false, "E-mail inválido" )
        }
        
    }
    
    func isValidCPF(_ str:inout String) -> (valid : Bool, error : String?) {
        
        if self.getLengthCPF(number: &str) < 11{
            return (false, "CPF inválido")
        }else{
            return (true, "")
        }
        
        
    }
    
    func isValidPhone(_ str:inout String) -> (valid : Bool, error : String?) {
        
        if self.getLengthPhone(number: &str) < 10{
            return (false, "Telefone inválido")
            
        }else{
            return (true, "")
        }
        
        
    }
    
    func isValidPassword(_ str:String?, otherString:String?) -> (valid : Bool, error : String?) {
        
        if str == otherString{
            return (true, "")
        }else{
            return (false, "Confirme sua senha")
        }
        
        
    }
    
    func isValidField(_ str:String?) -> (valid : Bool, error : String?) {
        
        if let string = str{
            if string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
                return (false, "Não preencheu o campo")
            }
        }else{
            return (false, "Não preencheu o campo")
        }
        return (true, "")
        
    }
    
    func formatNumberPhone( number:inout String) -> String
    {
        number = number.replacingOccurrences(of: "(", with: "")
        number = number.replacingOccurrences(of: ")", with: "")
        number = number.replacingOccurrences(of: " ", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        number = number.replacingOccurrences(of: "+", with: "")
        number = number.replacingOccurrences(of: ".", with: "")
        
        let length : Int  = number.count;
        if(length > 11)
        {
            number = number.substring(from: number.index(number.startIndex, offsetBy: length - 10))
        }
        
        return number;
    }
    
    
    
    func getLengthPhone(number:inout String) -> Int{
        number = number.replacingOccurrences(of: "(", with: "")
        number = number.replacingOccurrences(of: ")", with: "")
        number = number.replacingOccurrences(of: " ", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        number = number.replacingOccurrences(of: "+", with: "")
        number = number.replacingOccurrences(of: ".", with: "")
        return number.count;
        
    }
    
    func formatNumberCPF( number:inout String) -> String
    {
        number = number.replacingOccurrences(of: ".", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        
        
        return number;
    }
    func getLengthCPF(number:inout String) -> Int{
        number = number.replacingOccurrences(of: ".", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        
        return number.count;
        
    }

}
