//
//  UserDelegate.swift
//  DeVanPraEscolaVan
//
//  Created by App on 5/24/16.
//  Copyright © 2016 DVPE. All rights reserved.
//

import Foundation
import UIKit
@objc protocol FRUserAuthDelegate : NSObjectProtocol {
    
     @objc optional func login()
   
}

protocol FRUserAuthLogoutDelegate : NSObjectProtocol {
    
     func logout()
    
}


extension FRUserAuthLogoutDelegate where Self : UIViewController{
    func logout() {
        UserDefaultsUtil.sharedInstance.resetUserLogged()
        
        
        let vcLogin = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        
        if let window = UIApplication.shared.keyWindow{
            window.rootViewController = vcLogin
        }
    }
    
    func askLogout(){
        
        let alertController = UIAlertController(title: "Atenção", message: "Você deseja mesmo sair?", preferredStyle: .alert)
        self.customDefault(alertController, title: "Atenção", message: "Você deseja mesmo sair?")
        let defaultAction = UIAlertAction(title: "Não", style: .default, handler: nil)
        let otherActionHandler = { (action:UIAlertAction!) -> Void in
            self.logout()
        }
        let otherAction = UIAlertAction(title: "Sim", style: .default, handler: otherActionHandler)
        alertController.addAction(defaultAction)
        alertController.addAction(otherAction)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func customDefault(_ alertViewController : UIAlertController, title : String, message : String?){
        alertViewController.setValue(NSAttributedString(string: title.uppercased(), attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.init(red: 240.0/255.0, green: 147.0/255.0, blue: 85.0/255.0, alpha: 1.0)
            ])
            , forKey: "attributedTitle")
        if let m = message{
            alertViewController.setValue(NSAttributedString(string: m, attributes: [
                NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
                NSAttributedStringKey.foregroundColor : UIColor.init(red: 240.0/255.0, green: 147.0/255.0, blue: 85.0/255.0, alpha: 1.0)
                ])
                , forKey: "attributedMessage")
        }
        
        alertViewController.view.tintColor = UIColor.init(red: 28/255.0, green: 26/255.0, blue: 55/255.0, alpha: 1)
        
        
    }

}
