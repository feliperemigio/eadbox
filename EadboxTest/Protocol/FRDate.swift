//
//  FRDate.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation

extension DateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}

extension Foundation.Date {
    struct Date {
        static let formatterShortDate = DateFormatter(dateFormat: "yyyy-MM-dd HH:mm:ss")
    }
    var toString: String {
        return Date.formatterShortDate.string(from: self)
    }
    func period(_ untilDate : Foundation.Date) -> String{
        let firstMonth = self.month( self.getMonth())
        let secondMonth = self.month( untilDate.getMonth())
        
        let untilDay = untilDate.getDay() + 1
        let untilYear = untilDate.getYear()
        
        let day = self.getDay() + 1
        let year = self.getYear()
        if firstMonth != secondMonth && year != untilYear{
            return String(day) + " de " + firstMonth + " de " + String(year) + " à " + String(untilDay) + " de " + secondMonth + " de " + String(untilYear)
        }else  if firstMonth != secondMonth{
              return String(day) + " de " + firstMonth + " à " + String(untilDay) + " de " + secondMonth + " de " + String(untilYear)
        }else{
            return String(day) + " à " + String(untilDay) + " de " + secondMonth + " de " + String(year)
        }
        
    }
  
    func dateWithString(_ stringDate: String, formatString : String) -> Foundation.Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        if let date = dateFormatter.date(from: stringDate){
            return date
        }else{
            return Foundation.Date.init()
        }
        
    }
    
    func dateWithString(_ stringDate: String) -> Foundation.Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        if let date = dateFormatter.date(from: stringDate){
           return date
        }else{
            return Foundation.Date.init()
        }
        
    }
    func dayOfWeekString() -> String{
        return self.dayOfWeekByDay(self.dayOfWeek())
    }
    func hour() -> String{
        let formt: DateFormatter = DateFormatter()
        formt.dateFormat = "HH:mm:ss"
        return formt.string(from: self)
    }
    func dayOfWeek() -> Int{
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(.weekday, from: self)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    func getDay() -> Int{
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(.day, from: self)
        let day = myComponents.day
        return day!
    }
    func getMonth() -> Int{
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(.month, from: self)
        let month = myComponents.month
        return month!
    }
    func getYear() -> Int{
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(.year, from: self)
        let year = myComponents.year
        return year!
    }
    func dayOfWeekByDay(_ day : Int) -> String{
        switch day {
        case 1:
            return "domingo"
        case 2:
            return "segunda"
        case 3:
            return "terça"
        case 4:
            return "quarta"
        case 5:
            return "quinta"
        case 6:
            return "sexta"
        case 7:
            return "sábado"
        default: break
            
       
        }
        return ""
    }
    
    func month(_ month : Int) -> String{
        switch month {
        case 1:
            return "janeiro"
        case 2:
            return "fevereiro"
        case 3:
            return "março"
        case 4:
            return "abril"
        case 5:
            return "maio"
        case 6:
            return "junho"
        case 7:
            return "julho"
        case 8:
            return "agosto"
        case 9:
            return "setembro"
        case 10:
                return "outubro"
        case 11:
            return "novembro"
        case 12:
            return "dezembro"
            
        default: break
            
            
        }
        return ""
    }
    
    func getDateBrazil() -> String{
        return String(format: "%02d/%02d/%04d", self.getDay(), self.getMonth(), self.getYear())
    }

    func getDate() -> String{
        return String(format: "%04d-%02d-%02d", self.getYear(), self.getMonth(), self.getDay())
    }

    
    
}
