//
//  FRAlertView.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
import UIKit
protocol FRAlertView : NSObjectProtocol{
    
}

extension FRAlertView  where Self : UIViewController{
    
    func alertDefault(_ title : String?, message : String?){
        let alertController : UIAlertController =  UIAlertController.init(title: title, message: message, preferredStyle:.alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        self.customDefault(alertController, title: title, message: message)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
 
    fileprivate func customDefault(_ alertViewController : UIAlertController, title : String?, message : String?){
        if(title != nil){
            alertViewController.setValue(NSAttributedString(string: title!.uppercased(), attributes: [
                NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
                NSAttributedStringKey.foregroundColor : UIColor.init(red: 240.0/255.0, green: 147.0/255.0, blue: 85.0/255.0, alpha: 1.0)
                ])
                , forKey: "attributedTitle")
        }
        
        
        alertViewController.view.tintColor = UIColor.init(red: 28/255.0, green: 26/255.0, blue: 55/255.0, alpha: 1)
    }

}
