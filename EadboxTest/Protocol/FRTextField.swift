//
//  FRTextField.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit

extension UITextField{
    func padding(){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func addBorder(){
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.init(red: 140/255.0, green: 140/255.0, blue: 140/255.0, alpha: 1).cgColor
        self.layer.borderWidth = 1.0
    }
    


}
