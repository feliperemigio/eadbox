//
//  URIs.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
open class URIs {
     // MARK: Extensions
    struct extensions {
        static let json : String = ".json"
    }
    // MARK: Users
    static let USER : String = ""
   struct user {
        static let login : String = USER+"login"
    }
    
    // MARK: Courses
    static let COURSE : String = "admin/courses"
    struct course {
        static let list : String = COURSE
    }
    
}
