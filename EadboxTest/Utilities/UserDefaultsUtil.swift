//
//  UserDefaultsUtil.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
class UserDefaultsUtil {
    
    static let sharedInstance = UserDefaultsUtil()
    
    // MARK: Getters
    
    func userLogged()-> User{
        let defaults = UserDefaults.standard
        if let savedUser = defaults.object(forKey: "user") as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: savedUser) as! User
        }else{
            return User()
        }
    }
    
    // MARK: Setters
    
    func resetUserLogged(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "user")
    }
    
    func setUserLogged(_ user: User){
        let savedData = NSKeyedArchiver.archivedData(withRootObject: user)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: "user")
    }
    
  
    
    
    
}
