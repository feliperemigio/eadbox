//
//  CourseTableViewCell.swift
//  EadboxTest
//
//  Created by App on 07/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class CourseTableViewCell: UITableViewCell {
    var course : Course = Course()
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    func update(){
        lblTitle.text = course.title.uppercased()
        lblCategory.text = course.category.title.uppercased()
        self.img.image = nil
         if let url = URL.init(string: course.img){
            self.img.af_setImage(withURL: url)
        }
    }
    
}
