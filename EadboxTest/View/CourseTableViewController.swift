//
//  CourseTableViewController.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit

class CourseTableViewController: UITableViewController , FRAlertView, FRUserAuthLogoutDelegate{
    
    var page : Int32 = 1
    
    var courses : [Course] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requestData();
    }
    
     func requestData(){
        
        CourseAPI.sharedInstance.list(self.page, completion: { (result) in
            switch result
            {
            case .success(let courses):
                self.courses = courses
                self.tableView.reloadData()
                
            case .failure(let error):
                self.alertDefault("Ops!", message: error.localizedDescription)
                
            }
            
        }
            
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: IBActions
    @IBAction func didTouchLogout(_ sender: Any) {
         self.askLogout()
    }
    @IBAction func didTouchRefresh(_ sender: Any) {
        self.requestData()
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    // MARK: TableView
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.courses.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : CourseTableViewCell = CourseTableViewCell.init()
        
        if let cellButton : CourseTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CourseTableViewCell", for: indexPath) as? CourseTableViewCell{
            
            cell = cellButton
            cell.course = self.courses[indexPath.row];
            cell.update()
            
        }
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
    }
    
    
    
    
}
