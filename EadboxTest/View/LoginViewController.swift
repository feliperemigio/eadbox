//
//  ViewController.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit
class LoginViewController: UIViewController, UITextFieldDelegate, FRValidateForm, FRAlertView {
    fileprivate  var keyboardFrame: CGRect = CGRect();
    fileprivate  var  txtEditing: UITextField = UITextField();
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var btnEnter: UIButton!
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector:#selector(LoginViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(LoginViewController.keyboardWillDisappear(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(LoginViewController.hideKeyboard))
        
        self.scrollView.addGestureRecognizer(gestureRecognizer)
        
        self.txtEmail.addBorder()
        self.txtEmail.padding()
        self.txtPassword.addBorder()
        self.txtPassword.padding()
        
    }
    //MARK: IBActions
    @IBAction func didTouchEnter(_ sender: AnyObject) {
        
        if !self.validate().valid{
            return
        }
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.btnEnter?.isEnabled = false
        let user = User.init(email: txtEmail.text, password: txtPassword.text)
        
        self.hideKeyboard()
        
        LoginAPI.sharedInstance.login(user, completion: { (result) in
            switch result
            {
            case .success(let user):
                
                if  user.apiKey.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
                    self.alertDefault("Ops!", message: "Login inválido")
                    return
                }
                UserDefaultsUtil.sharedInstance.setUserLogged(user)
                
                let vcMain = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CourseNavigationController")
                vcMain.loadView()
                if let window = UIApplication.shared.keyWindow{
                    window.rootViewController = vcMain
                }
                
            case .failure(let error):
                self.alertDefault("Ops!", message: error.localizedDescription)
                
            }
            self.activityIndicatorView.stopAnimating()
            self.btnEnter?.isEnabled = true
        }
        )
        
        
    }
    //MARK: Keyboard
    @objc fileprivate func keyboardWillShow(_ notification:Notification ){
        
        self.keyboardFrame = ((notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        self.scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: self.scrollView.subviews.last?.subviews.last?.frame.maxY ?? 0+20+self.keyboardFrame.size.height)
        
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            let p = self.txtEditing.convert(self.txtEditing.center, to: self.scrollView.subviews[0])
            
            self.scrollView.contentOffset = CGPoint(x: self.scrollView.contentOffset.x, y:p.y-self.keyboardFrame.size.height+self.txtEditing.frame.size.height)
            
        })
        
    }
    @objc fileprivate func keyboardWillDisappear(_ notification:Notification ){
        self.scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: self.scrollView.subviews.last?.subviews.last?.frame.maxY ?? 0+20)
        
    }
    @objc fileprivate func hideKeyboard(){
        self.txtEmail.resignFirstResponder()
        self.txtPassword.resignFirstResponder()
    }
    
    //MARK: UIextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        }else  if textField == self.txtPassword {
            self.didTouchEnter(self)
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.txtEditing = textField;
    }
    
    //MARK: Validate
    func validate() -> (valid: Bool, error : String?){
        
        if(txtEmail.text != nil && txtPassword.text != nil ){
            if(txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" && txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" ){
                if(self.isValidEmail(txtEmail.text).valid){
                    return (true, "")
                }else{
                    self.alertDefault("Ops!", message: "Email inválido")
                }
            }else{
                self.alertDefault("Ops!", message: "Preencha todos os campos")
            }
        }else{
            self.alertDefault("Ops!", message: "Preencha todos os campos")
        }
        
        return (false, "Preencha os campos corretamente")
    }
    
    
    
    
}



