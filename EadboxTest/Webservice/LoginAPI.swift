//
//  LoginApi.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
import Alamofire
class LoginAPI : NSObject,  FRNetworking {
     static let sharedInstance = LoginAPI()
    fileprivate var API : String? = ""
    override init() {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            if let data = NSDictionary(contentsOfFile: path){
                API = data.value(forKey: "FRApi")  as? String
            }
        }
      
    }
    func login(_ user : User, completion:  @escaping (FRAsyncResult<User>)->()){
        post((API != nil ? API! + URIs.USER : "")+"login", parameters: ["email":user.email,
            "password":user.password], completion: { (result) in
            switch result
            {
            case .success(let data):
                let dic : NSDictionary? = data as? NSDictionary
                if(dic != nil){
                    completion(FRAsyncResult.success(User.init(object: dic)))
                }else{
                     completion(FRAsyncResult.success(User.init()))
                }

                break
            case .failure(let error):
                completion(FRAsyncResult.failure(error))
                break
            }
        }
        )
    }
    
   

}
