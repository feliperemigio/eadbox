//
//  FRAsyncResult.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
enum FRAsyncResult<T>
{
    case success(T)
    case failure(Error)
}
