//
//  CourseAPI.swift
//  EadboxTest
//
//  Created by App on 07/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit
import Alamofire
class CourseAPI: NSObject,  FRNetworking {
    static let sharedInstance = CourseAPI()
    fileprivate var API : String? = ""
    override init() {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            if let data = NSDictionary(contentsOfFile: path){
                API = data.value(forKey: "FRApi")  as? String
            }
        }
        
    }
    func list(_ page : Int32, completion:  @escaping (FRAsyncResult<[Course]>)->()){
        let parameter = ["auth_token":UserDefaultsUtil.sharedInstance.userLogged().apiKey, "page":String(page)]
        
        get((API != nil ? API! + URIs.course.list  : ""), parameters: parameter, completion: { (result) in
            switch result
            {
            case .success(let data):
                if let array = data as? NSArray{
                    var arrayCourses : [Course] = []
                    for dic in array{
                        if let dicCourse : NSDictionary = dic as? NSDictionary{
                            arrayCourses.append(Course.init(object: dicCourse))
                        }
                    }
                    completion(FRAsyncResult.success(arrayCourses))
                }else{
                    let error : NSError = NSError.init(domain: customError, code: Code.badRequest.rawValue, userInfo: [NSLocalizedDescriptionKey:"Nenhuma mensagem"])
                    completion(FRAsyncResult.failure(error))
                }
                break
            case .failure(let error):
                completion(FRAsyncResult.failure(error))
                break
            }
        }
        )
    }
}
