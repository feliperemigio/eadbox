//
//  FRNetworking.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation
import Alamofire
let customError : String = "br.com.eadbox.custom.ErrorDomain"
enum Code : Int{
    case unauthorize = 0
    case badRequest = 2
}

protocol FRNetworking {
    
}

extension FRNetworking{
    func get(_ url : String,
             parameters: [String : Any]?,
             completion:  @escaping (FRAsyncResult<Any?>)->()){
        
        Alamofire.request(url, method: .get, parameters:  parameters ).validate(statusCode: 200...590)
            .responseJSON { response in
                switch response.result {
                case .success:
                    self.completion(response, completion: {  (result) in
                        completion(result)
                    })
                    break
                case .failure(let error):
                    completion(FRAsyncResult.failure(error))
                    break
                }
        }
        
    }
    
    func post(_ url : String,
              parameters: [String : Any]?,
              completion:  @escaping (FRAsyncResult<Any?>)->()){
        
        Alamofire.request(url, method: .post, parameters:  parameters ).validate(statusCode: 200...590)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    self.completion(response, completion: {  (result) in
                            completion(result)
                        })
                case .failure(let error):
                    
                    completion(FRAsyncResult.failure(self.error(response.response, error: error)))
                    break
                }
        
        }
        
    }
    
    
    fileprivate func completion(_ response: DataResponse<Any> , completion:  (FRAsyncResult<Any?>)->()){
        if let statusCode = response.response?.statusCode{
            
            if statusCode == 200{
                if let JSON = response.result.value {
                    completion(FRAsyncResult.success(JSON))
                }else{
                    completion(FRAsyncResult.failure(response.result.error!))
                }
            }else{
                if let JSON = response.result.value {
                    if  let dic : NSDictionary = JSON as? NSDictionary{
                        if  let message : String = (dic.object(forKey: "data") as AnyObject).object(forKey: "message") as? String{
                            let error = NSError.init(domain: customError, code: Code.unauthorize.rawValue, userInfo: [NSLocalizedDescriptionKey:message])
                            completion(FRAsyncResult.failure(error))
                        }else{
                            
                            completion(FRAsyncResult.failure(self.errorDefault()))
                        }
                    }else{
                        
                        completion(FRAsyncResult.failure(self.errorDefault()))
                    }
                    
                }else{
                    completion(FRAsyncResult.failure(response.result.error!))
                }
            }
            
        }

    }
    
    fileprivate func errorDefault() -> Error{
        return NSError.init(domain: customError, code: Code.badRequest.rawValue, userInfo: [NSLocalizedDescriptionKey:"Ocorreu um problema, tente novamente mais tarde"])
    }
    
    fileprivate func error(_ response : HTTPURLResponse?, error : Error) -> Error{
        
        if let httpStatusCode  = response?.statusCode {
            if(httpStatusCode == 401){
                return NSError.init(domain: customError, code: Code.unauthorize.rawValue, userInfo: [NSLocalizedDescriptionKey:"Login inválido"])
            }else{
                return error
            }
        } else {
            return error
        }
    }
    
    
}
