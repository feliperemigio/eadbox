//
//  User.swift
//  EadboxTest
//
//  Created by App on 06/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation

class User : NSObject, NSCoding {
    var id : String = ""
    var name : String = ""
    var password : String = ""
    var email : String = ""
    var apiKey : String = ""
    var cpf : String = ""
    
    override init(){
        
    }
    
    required init(coder aDecoder: NSCoder) {
        if let id  = aDecoder.decodeObject(forKey: "id") as? String{self.id = id}
        if let name  = aDecoder.decodeObject(forKey: "name") as? String{self.name = name}
        if let email  = aDecoder.decodeObject(forKey: "email") as? String{self.email = email}
        if let password  = aDecoder.decodeObject(forKey: "password") {self.password = password as! String}
        if let apiKey  = aDecoder.decodeObject(forKey: "apiKey") as? String{self.apiKey = apiKey}
        if let cpf  = aDecoder.decodeObject(forKey: "cpf") as? String{self.cpf = cpf }
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name, forKey: "name")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(apiKey, forKey: "apiKey")
        aCoder.encode(cpf, forKey: "cpf")
        
    }
    
    init(object : NSDictionary?){
        if(object?.value(forKey: "user") != nil ){
            let user = object?.value(forKey: "user") as? NSDictionary
            if let id = user?.value(forKey: "user_id") as? String{self.id = id}
            if let name = user?.value(forKey: "name") as? String{self.name = name}
            if let email = user?.value(forKey: "email") as? String{self.email = email}
            if let apiKey = object?.value(forKey: "authentication_token") as? String{self.apiKey = apiKey}
        }
        
    }
    
    init(email : String?, password : String?){
        if(email != nil){ self.email = email! }
        if(password != nil){ self.password = password!}
    }
    
    func save(){
        let savedData = NSKeyedArchiver.archivedData(withRootObject: self)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: "user")
    }
    
}
