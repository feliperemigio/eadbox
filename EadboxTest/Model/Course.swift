//
//  Course.swift
//  EadboxTest
//
//  Created by App on 07/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import Foundation

class Course: NSObject, NSCoding {
    var id : String = ""
    var title : String = ""
    var img : String = ""
    var category : Category = Category()
    override init(){
    }
    
    init(title : String){
        self.title = title
    }
    
    init(object : NSDictionary?){
        if let id = object?.value(forKey: "course_id") as? String{self.id = id}
        if let title = object?.value(forKey: "title") as? String{self.title = title}
        if let img = object?.value(forKey: "logo_url") as? String{self.img = img}
         if let category = object?.value(forKey: "category") as? NSDictionary{self.category = Category.init(object: category)}
    }
    required init(coder aDecoder: NSCoder) {
        if let title  = aDecoder.decodeObject(forKey: "title") as? String{self.title = title}
        if let id  = aDecoder.decodeObject(forKey: "id") as? String{self.id = id}
        if let img  = aDecoder.decodeObject(forKey: "img") as? String{self.img = img}
        if let category  = aDecoder.decodeObject(forKey: "category") as? Category{self.category = category}
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(img, forKey: "img")
        aCoder.encode(category, forKey: "category")
        
    }
}
