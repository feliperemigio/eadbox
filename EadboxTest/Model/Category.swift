//
//  Course.swift
//  EadboxTest
//
//  Created by App on 07/06/18.
//  Copyright © 2018 eadbox. All rights reserved.
//

import UIKit

class Category: NSObject, NSCoding {
    var id : String = ""
    var title : String = ""
    override init(){
    }
    
    init(title : String){
        self.title = title
    }
    
    init(object : NSDictionary?){
        if let id = object?.value(forKey: "category_id") as? String{self.id = id}
        if let title = object?.value(forKey: "title") as? String{self.title = title}
    }
    
    required init(coder aDecoder: NSCoder) {
        if let title  = aDecoder.decodeObject(forKey: "title") as? String{self.title = title}
        if let id  = aDecoder.decodeObject(forKey: "id") as? String{self.id = id}
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(id, forKey: "id")
    }

}
